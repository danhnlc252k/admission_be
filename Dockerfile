FROM node:14.18.1-alpine

WORKDIR /app

# RUN adduser -S app

COPY package*.json .

# RUN npm install --production --silent
RUN npm install --production=true

COPY . .

# RUN chown -R app /app

# USER app

EXPOSE 3000

RUN addgroup -g 197609 appgroup

RUN adduser -D -u 197609 appuser -G appgroup

RUN chown -R appuser:appgroup /app

USER appuser

# CMD ["pm2-runtime", "ecosystem.config.js", "--env", "production"]
CMD [ "npm", "run", "pm2" ]
